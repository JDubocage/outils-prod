# Outils de productions
## TP2
### Création des features branches

[Lien du GitLab](https://gitlab.com/JDubocage/outils-prod.git)


Commandes utilisées : 
- Creation des deux branches : `git branch ajout-styles && git branch modification-textes`
- Passer ajout-styles en banche courrante : `git switch ajout-styles`
- Ajout des fichiers à l'index : `git add index.html styles.css`
- Création du commit : `git commit -m "feat(tp2): ajout de style blue pour les paragraphes"`
- On repasse master en branche courrante : `git switch master`
- Vérifier qu'on ne voit pas le dernier commit  : `git log`
- Passer ajout-styles en banche courrante : `git switch modification-textes`
- Ajout des fichiers à l'index : `git add index.html`
- Création du commit : `git commit -m "fix(tp2): changement texte du paragraphe"`
- Ajout des fichiers à l'index : `git add index.html`
- Création du commit : `git commit -m "fix(tp2): changement titre document"`

### Rapatrier le travail sur la branche master

Commandes utilisées : 
- On se place sur master : `git switch master`
- Rapatrier les changements de *modification-textes* : `git merge modification-textes`
- Consulter l'historique de la branche master : `git log`
- Rapatrier les changements de *ajout-styles* : `git merge ajout-styles`
- Etat du dépôt : `git status`
- Fix du conflict puis commit : `git commit -m "fix(tp2): fusion de la branche ajout-styles dans master"`
- Affichage de l'historique : `git log --graph`

## Partager son projet

Commandes utilisées : 
- Ajout du remote : `git remote add origin https://gitlab.com/JDubocage/outils-prod.git`
- Push des branches : `git push origin --all`

## Nettoyer le dépôt local
Commandes utilisées : 
- Suppression des branches inutiles : `git branch -d ajout-styles && git branch -d modification-textes`
- Vérifier qu'il ne reste plus qu'une branche (master) : `git branch`

### Questions

1. Puisqu'on a mergé une branche de feature dans une branche parente qui n'a pas évoluée depuis les modifications, 
git ne créer pas de merge commit et c'est la branche courrante (*master*) qui a été déplacée au niveau de la branche à fusionner (*modification-textes*). C'est ce qui s'appelle fast forward
2. Un conflit est apparu lors du deuxieme merge car la branche master a évolué entre le moment ou la branche modification-textes a été créer et le moment où on a voulu la merge. Il y avait donc deux modifications (ou versions)concurrentes sur le fichier index.html.
3. Oui on peux : Il faut recréer une branche et faire `git merge` suivi de *l'id* du commit de la branche que l'on souhaite récupérer (`git reflog` peut être utile pour retrouver cet id)
4. Cela permet de visualiser de manière simple les lignes de vie de chaque branche et quand elles se rejoignent
